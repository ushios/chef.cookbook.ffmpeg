#
# Cookbook Name:: ffmpeg
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

directory node[:ffmpeg][:wd] do
	action :create
	owner 'root'
	group 'root'
	mode 0755
end
