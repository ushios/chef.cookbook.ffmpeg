#
# Cookbook Name:: ffmpeg
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

dir = node[:ffmpeg][:wd];

execute 'build-dep ffmpeg' do
	command 'apt-get -y build-dep ffmpeg' + ' && ' + "touch #{dir}/.build_dep"
	creates dir + '/.build_dep'
end
