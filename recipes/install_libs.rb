#
# Cookbook Name:: ffmpeg
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package 'libmp3lame-dev libfaac-dev libx264-dev libxvidcore-dev libdirac-dev' do
	action :install
end
