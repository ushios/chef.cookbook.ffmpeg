#
# Cookbook Name:: ffmpeg
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


include_recipe "ffmpeg::mkdir"
include_recipe "ffmpeg::tools"
include_recipe "ffmpeg::install_libs"
include_recipe "ffmpeg::build_dep"
include_recipe "ffmpeg::install"
#include_recipe "ffmpeg::ubuntu_desktop"
