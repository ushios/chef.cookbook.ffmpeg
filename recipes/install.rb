#
# Cookbook Name:: ffmpeg
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

dir = node[:ffmpeg][:wd];

wd = dir + '/ffmpeg_git'

git wd do
	repository node[:ffmpeg][:repository]
	reference node[:ffmpeg][:revision]
	action :sync
end

execute 'configure' do
	command './configure --prefix=/usr/local --enable-gpl --enable-postproc --enable-swscale --enable-avfilter --enable-libmp3lame --enable-libvorbis --enable-libtheora --enable-libdirac --enable-libschroedinger --enable-libfaac --enable-libxvid --enable-libx264 --enable-libvpx --enable-libspeex --enable-nonfree --enable-shared --enable-pthreads && ' + "touch #{dir}/.configure"
	action :run
	cwd wd
	creates dir + '/.configure'
end

execute 'make' do
	command 'make -j' + node[:ffmpeg][:make][:jobs] + ' && ' + "touch #{dir}/.make"
	action :run
	cwd wd
	creates dir + '/.make'
end

execute 'make install' do
	command 'make install' + ' && ' + "touch #{dir}/.make_install"
	action :run
	cwd wd
	creates dir + '/.make_install'
end

template '/etc/ld.so.conf.d/ffmpeg_10.conf' do
	source 'ffmpeg_10.conf.erb'
	owner 'root'
	group 'root'
	mode 0644
end

execute 'reload conf' do
	command 'sudo /sbin/ldconfig' + ' && ' + "touch #{dir}/.ldconfig"
	action :run
	creates dir + '/.ldconfig'
end
