default[:ffmpeg][:wd] = "/var/chef/ffmpeg"
default[:ffmpeg][:repository] = "git://source.ffmpeg.org/ffmpeg.git"
default[:ffmpeg][:revision] = "n0.10.2"
default[:ffmpeg][:make][:jobs] = "2"
